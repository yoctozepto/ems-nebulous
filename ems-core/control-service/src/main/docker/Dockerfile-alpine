#
# Copyright (C) 2017-2023 Institute of Communication and Computer Systems (imu.iccs.gr)
#
# This Source Code Form is subject to the terms of the Mozilla Public License, v2.0, unless 
# Esper library is used, in which case it is subject to the terms of General Public License v2.0.
# If a copy of the MPL was not distributed with this file, you can obtain one at 
# https://www.mozilla.org/en-US/MPL/2.0/
#

ARG BUILDER_IMAGE=eclipse-temurin:17.0.8_7-jre-alpine
ARG RUN_IMAGE=eclipse-temurin:17.0.8_7-jre-alpine

# ----------------- Builder image -----------------
FROM $BUILDER_IMAGE as ems-server-builder
#FROM vegardit/graalvm-maven:latest-java17
WORKDIR /app
COPY jars/control-service.jar .
RUN java -Djarmode=layertools -jar control-service.jar extract

# -----------------   Run image   -----------------
FROM $RUN_IMAGE

# Setup environment
ENV BASEDIR /opt/ems-server
ENV EMS_HOME ${BASEDIR}
ENV EMS_CONFIG_DIR ${BASEDIR}/config

ENV BIN_DIR ${BASEDIR}/bin
ENV CONFIG_DIR ${BASEDIR}/config
ENV LOGS_DIR ${BASEDIR}/logs
ENV PUBLIC_DIR ${BASEDIR}/public_resources

# Install required and optional packages
RUN apk update && apk add \
    dumb-init curl bash \
    netcat-openbsd \
    vim \
    iputils-ping

# Add an EMS user
ARG EMS_USER=emsuser
RUN mkdir ${EMS_HOME} ; \
    addgroup ${EMS_USER} ; \
    adduser --home ${EMS_HOME} --no-create-home --ingroup ${EMS_USER} --disabled-password ${EMS_USER} ; \
    chown ${EMS_USER}:${EMS_USER} ${EMS_HOME}

USER ${EMS_USER}
WORKDIR ${BASEDIR}

# Download a JRE suitable for running EMS clients, and
# offer it for download
ENV JRE_LINUX_PACKAGE zulu17.44.15-ca-jre17.0.8-linux_x64.tar.gz
RUN mkdir -p ${PUBLIC_DIR}/resources
RUN curl https://cdn.azul.com/zulu/bin/${JRE_LINUX_PACKAGE} --output ${PUBLIC_DIR}/resources/${JRE_LINUX_PACKAGE}

# Copy resource files to image
ADD --chown=${EMS_USER}:${EMS_USER} bin ${BIN_DIR}
ADD --chown=${EMS_USER}:${EMS_USER} config ${CONFIG_DIR}
ADD --chown=${EMS_USER}:${EMS_USER} public_resources ${PUBLIC_DIR}

RUN mkdir ${LOGS_DIR}
RUN chmod +rx ${BIN_DIR}/*.sh

# Copy files from builder container
COPY --chown=${EMS_USER}:${EMS_USER} --from=ems-server-builder /app/dependencies          ${BASEDIR}
COPY --chown=${EMS_USER}:${EMS_USER} --from=ems-server-builder /app/spring-boot-loader    ${BASEDIR}
COPY --chown=${EMS_USER}:${EMS_USER} --from=ems-server-builder /app/snapshot-dependencies ${BASEDIR}
COPY --chown=${EMS_USER}:${EMS_USER} --from=ems-server-builder /app/application           ${BASEDIR}

# Copy ESPER dependencies
COPY --chown=${EMS_USER}:${EMS_USER} jars/esper*.jar ${BASEDIR}/BOOT-INF/lib/

EXPOSE 2222
EXPOSE 8111
EXPOSE 61610
EXPOSE 61616
EXPOSE 61617

ENTRYPOINT ["dumb-init", "./bin/run.sh"]